=== Open Government Publications - Client ===

Requires at least: WordPress 4.8
Tested up to: WordPress 5.2.2
Stable tag: 1.0.1
Version: 1.0.1

## Changelog

## 1.0.1: 29 October 2019
- Added: update class
- Fixed: grammar error

## 1.0.0: 1 October 2019
- Initial release